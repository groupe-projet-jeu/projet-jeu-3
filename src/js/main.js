'use strict';

import css from '../css/main.css';
import playScreen from '../html/playScreen.html';
import '../../node_modules/bootstrap/scss/bootstrap.scss';

import story from '../json/story.json';
import data from '../json/scene6.json';
import { Scenery } from './scenery';

let main = document.querySelector('main');
main.innerHTML = playScreen;

// console.log(storyArray);

// import { Player } from './player';
// let player = new Player('./images/character/worker.png', 3, 'strong');

let scene = new Scenery(data);
scene.displayScene();

// console.log(player);
// player.displayLife();
// player.looseLife();
// player.displayLife();

// This is useless code because we added the httml files in an other tag, but it may be useful !
// ————————————
// let script = document.createElement('script');
// script.src = '../dist/bundle.js';
// body.appendChild(script);