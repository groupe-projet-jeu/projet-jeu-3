"use strict";

// We import the html page with the raw-loader.
import win from '../html/win.html';
import loose from '../html/loose.html';
import playScreen from '../html/playScreen.html';

import data from '../json/scene5.json';

import { ChoiceItem } from './choiceItem';
import { Item } from "./item";

// We prefer to attribute to the property the result of a method to be able to easily test our class instances.
export class Scenery {
  constructor(data) {
    this.name = data.name;
    this.listItems = this.getListItems(data);
    this.listChoices = this.getListChoices(data);
    this.solution = data.solution;
    this.reward = data.reward;
  }

  getListItems(data) {
    let listItems = [];
    for (let i = 0; i < data.listItems.length; i++) {
      let dataX = data.listItems[i];
      let item = new Item(dataX.name, dataX.kind, `rgb(214, 214, 214)`, dataX.isClue);
      listItems.push(item);
    }
    return listItems;
  }

  getListChoices(data) {
    let listChoices = [];
    for (let i = 0; i < data.listChoices.length; i++) {
      let dataX = data.listChoices[i];
      let item = new ChoiceItem(dataX.name, dataX.kind, this.getColorItems(data, i), dataX.isClue, dataX.description);
      listChoices.push(item);
    }
    return listChoices;
  }

  displayScene() {
    let main = document.querySelector('main');
    let scene = document.querySelector('#scenery');
    let choice = document.querySelector('#choice');
    let submitChoice = document.querySelector('#submitChoice');

    for (let i = 0; i < this.listItems.length; i++) {
      let item = this.listItems[i];

      let div = document.createElement('div');
      let img = document.createElement('img');

      if (item.isClue === false) {
        img.src = item.image;
        img.alt = item.name;
        div.style.backgroundColor = item.color;
      } else {
        img.src = `./images/${item.kind}/question.png`;
        img.alt = `clue`;
        div.style.backgroundColor = 'rgb(214, 214, 214)';
      }
      img.style.height = "8vw";

      div.setAttribute('class', `d-flex flex-wrap align-content-center justify-content-center item col-2`);

      div.appendChild(img);
      scene.appendChild(div);
    }

    for (let i = 0; i < this.listChoices.length; i++) {
      let item = this.listChoices[i];

      let div = document.createElement('div');
      let img = document.createElement('img');

      img.src = item.image;
      img.alt = item.name;
      div.style.backgroundColor = item.color;

      img.style.height = "5vw";

      choice.setAttribute("class", "col-md-10 d-flex justify-content-center");
      div.setAttribute('class', `choice d-flex flex-wrap align-content-center justify-content-center item col-2`);

      div.addEventListener('click', function () {
        item.displayDescription(item);
      })

      // This button only appears when the user clicks on the different choices.
      div.addEventListener('click', () => {
        let submitBtn = document.createElement('button');
        submitChoice.innerHTML = '';
        submitBtn.textContent = 'Validation';
        submitBtn.setAttribute('class', 'btn btn-success');
        submitChoice.appendChild(submitBtn);

        let selecItem = document.querySelector('.selected');

        if (selecItem) {
          selecItem.classList.remove('selected');
        }

        div.classList.add('selected');

        submitBtn.addEventListener('click', () => {
          // The winning page is loaded if the user has found the right answer.
          main.innerHTML = '';
          if (item.name === this.solution) {
            main.innerHTML = win;
            let nextButton = document.querySelector('#next');
            nextButton.addEventListener('click', () => {
              main.innerHTML = '';
              main.innerHTML = playScreen;
              let nextScene = new Scenery(data);
              nextScene.displayScene();
            })
          } else {
            main.innerHTML = loose;
          }
        })
      });

      div.appendChild(img);
      choice.appendChild(div);
    };

  }

  // This method returns a number that is then used in css in an hsl value. Very useful !
  // http://guepe.ateliez.fr/Shaarli/?JqzkeQ
  getColorItems(data, i) {
    let percent = 180 / data.listChoices.length;
    let result = percent * i;

    return result;
  }
}